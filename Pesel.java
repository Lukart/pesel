package pl.sdacademy.lesson9.homework.pesel;

import java.util.Scanner;

public class Pesel {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Set PESEL Number: ");
        String pesel = scanner.nextLine();

        int year = Integer.parseInt(pesel.substring(0, 2));
        int month = Integer.parseInt(pesel.substring(2, 4));
        int day = Integer.parseInt(pesel.substring(4, 6));
        int liczbaPorzadkowa = Integer.parseInt(pesel.substring(6, 10));
        int sex = Integer.parseInt(pesel.substring(8, 9));
        int controlNumber = Integer.parseInt(pesel.substring(10));

        month(month, year);
        day(day);
        sex(sex);
        controlNumber(pesel, controlNumber);
        liczbaPorzadkowa(liczbaPorzadkowa);

    }

    private static void liczbaPorzadkowa(int liczbaPorzadkowa) {
        System.out.println("Liczba porzadkowa: " + liczbaPorzadkowa);
    }

    private static void day(int day) {
        System.out.println("Day of born: " + day + "-th");
    }

    private static void monthName(int temp) {
        if (temp == 1) {
            System.out.println("Month of born: January");
        } else if (temp == 2) {
            System.out.println("Month of born: February");
        } else if (temp == 3) {
            System.out.println("Month of born: March");
        } else if (temp == 4) {
            System.out.println("Month of born: April");
        } else if (temp == 5) {
            System.out.println("Month of born: May");
        } else if (temp == 6) {
            System.out.println("Month of born: June");
        } else if (temp == 7) {
            System.out.println("Month of born: July");
        } else if (temp == 8) {
            System.out.println("Month of born: August");
        } else if (temp == 9) {
            System.out.println("Month of born: September");
        } else if (temp == 10) {
            System.out.println("Month of born: October");
        } else if (temp == 11) {
            System.out.println("Month of born: November");
        } else if (temp == 12) {
            System.out.println("Month of born: December");
        }
    }

    private static void month(int month, int year) {

        int temp = 0;
        if (month > 0 && month <= 12) {
            System.out.println("Person born in: " + "19" + year + " year.");
            temp = month;
        } else if (month > 20 && month <= 32) {
            System.out.println("Person born in: " + "20" + year + " year.");
            temp = month - 20;
        } else if (month > 40 && month <= 52) {
            System.out.println("Person born in: " + "21" + year + " year.");
            temp = month - 40;
        } else if (month > 60 && month <= 72) {
            System.out.println("Person born in: " + "22" + year + " year.");
            temp = month - 60;
        } else if (month > 80 && month <= 92) {
            System.out.println("Person born in: " + "18" + year + " year.");
            temp = month - 80;
        } else {
            System.out.println("Wrong pesel");
        }
        monthName(temp);
    }

    private static void sex(int sex) {

        if (sex % 2 == 0) {
            System.out.println("Person: Woman");
        } else {
            System.out.println("Person: Man");
        }
    }

    private static void controlNumber(String pesel, int controlNumber) {
        int p0 = Integer.parseInt(pesel.substring(0, 1));
        int p1 = Integer.parseInt(pesel.substring(1, 2));
        int p2 = Integer.parseInt(pesel.substring(2, 3));
        int p3 = Integer.parseInt(pesel.substring(3, 4));
        int p4 = Integer.parseInt(pesel.substring(4, 5));
        int p5 = Integer.parseInt(pesel.substring(5, 6));
        int p6 = Integer.parseInt(pesel.substring(6, 7));
        int p7 = Integer.parseInt(pesel.substring(7, 8));
        int p8 = Integer.parseInt(pesel.substring(8, 9));
        int p9 = Integer.parseInt(pesel.substring(9, 10));

        int lastNumber = (10 - ((p0 + p1 * 3 + p2 * 7 + p3 * 9 + p4 + p5 * 3 + p6 * 7 + p7 * 9 + p8 +
                p9 * 3) % 10)) % 10;

        if (lastNumber == controlNumber) {
            System.out.println("Control Number is: correct");
        } else {
            System.out.println("Control Number is: incorrect");
        }
    }


}

